$(document).ready(function () {

    jQuery("#gallery").unitegallery({
        tiles_type:"nested",
        tiles_nested_optimal_tile_width:500
    });
 jQuery("#gallery2").unitegallery({
        tiles_type:"nested",
        tiles_nested_optimal_tile_width:600
    });
 jQuery("#gallery3").unitegallery({
        tiles_type:"nested",
        tiles_nested_optimal_tile_width:400
    });

    $('#clock').countdown('2020/10/10 18:1:0', function(event) {
        var $this = $(this).html(event.strftime(''
            +" <div class=\"row\">"
            +"<div class=\"col-sm-4 col-md-4 col-lg-4\">"
            + "<div class=\"clck_dgt\">%H</div>"
            +"<div class=\"clck_dsc\">Часы</div>"
            + "</div>"
            + "<div class=\"col-sm-4 col-md-4 col-lg-4\">"
            +"<div class=\"clck_dgt\">%M</div>"
            + "<div class=\"clck_dsc\">Минуты</div>"
            + "</div>"
            +"<div class=\"col-sm-4 col-md-4 col-lg-4\">"
            + "<div class=\"clck_dgt\">%S</div>"
            + "<div class=\"clck_dsc\">Секунды</div>"
            + "</div>"
            + "</div>"



        ));
    });



    $('#order_modal').on('show.bs.modal', function (event) {
        $("#success_message").css("display","none");
        $("#order_form").css("display","block");
        $("#createorder_in_process").css("display","none");
        $("#createorder").css("display","block");


        var button = $(event.relatedTarget);
        var deal_id = button.data('deal_id');
        var modal_msg_title = button.data('title');
        var modal_msg_price = button.data('price');
        //var modal = $(this);

        $('#deal_id').val(deal_id);
        $('#modal_msg_title').html(modal_msg_title);
        $('#modal_msg_price').html(modal_msg_price);
    });



    $("#createorder").click(function () {
        $("#createorder").css("display","none");
        $("#createorder_in_process").css("display","block");
        var data = {
            id: $("#deal_id").val(),
            a: 1,
            name: $("#customer_name").val(),
            phone: $("#customer_phone").val()
        };
        $.ajax({
            type: 'POST',
            url: "http://hazestore.ru/user_api/create_order",
            data: data,
            error: function (xhr) {
                // console.log("ERROR ON SUBMIT");
            },
            success: function (data) {
                if(data.status==1){
                    $("#order_form").css("display","none");
                    $("#success_message").css("display","block");


                    setTimeout(function(){
                        $('#order_modal').modal('hide')
                    }, 8000);
                }
            }
        });
    });


    $("#send_request").click(function () {
        //console.log("createorder");
        $("#send_request").css("display","none");
        $("#send_request_progress").css("display","block");
        var data = {
            id: 1096,
            a: 1,
            name: $("#request_customer_name").val(),
            phone: $("#request_customer_phone").val()
        };
        console.log("data",data);
        $.ajax({
            type: 'POST',
            url: "http://hazestore.ru/user_api/create_order",
            data: data,
            error: function (xhr) {
                // console.log("ERROR ON SUBMIT");
            },
            success: function (data) {
                console.log("data", data);
                if(data.status==1){
                    $("#request_form").css("display","none");
                    $("#send_request_success").css("display","block");
                }
            }
        });
    });





});